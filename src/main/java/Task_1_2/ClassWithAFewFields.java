package Task_1_2;

import java.util.Objects;

public class ClassWithAFewFields {
    @ValidData(defAge = 23)
    int age;
    String name;
    String status;

    public ClassWithAFewFields(int age, String name, String status) {
        this.age = age;
        this.name = name;
        this.status = status;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClassWithAFewFields that = (ClassWithAFewFields) o;
        return age == that.age &&
                Objects.equals(name, that.name) &&
                Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(age, name, status);
    }

    @Override
    public String toString() {
        return "ClassWithAFewFields{" +
                "age=" + age +
                ", name='" + name + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
