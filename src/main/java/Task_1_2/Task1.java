package Task_1_2;

import java.lang.reflect.Field;

public class Task1 {

    private static int anInt;

    public static void main(String[] args) {

        ClassWithAFewFields classWithAFewFields = new ClassWithAFewFields( 29, "oleksii", "KING");

        try {
            validate(classWithAFewFields);
            System.out.println(classWithAFewFields);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

    }

    public static void validate (ClassWithAFewFields classWithAFewFields) throws IllegalAccessException {
        Class<? extends ClassWithAFewFields> aClass = classWithAFewFields.getClass();
        Field[] fields = aClass.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            if(field.isAnnotationPresent(ValidData.class)){
                int age = (int) field.get(classWithAFewFields);
                if (age > 23){
                    ValidData validData = field.getAnnotation(ValidData.class);
                    int trueAge = validData.defAge();
                    field.set(classWithAFewFields, trueAge);
                }else{
                    System.out.println("error");
                }
            }
            
        }
    }
}
